import yaml
import d6tflow
import luigi
from google.cloud import bigquery
client = bigquery.Client()

with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)

d6tflow.settings.dir = 'data'
d6tflow.settings.log_level = cfg["d6tflow"]["log_level"]
luigi.configuration.core.logger.setLevel(cfg["luigi"]["log_level"])

