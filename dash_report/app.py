import dash, dash_auth, dash_core_components as dcc, dash_html_components as html
from dash.dependencies import Output, Input
import os, joblib, pandas as pd, plotly.graph_objs as go, plotly.express as p, dash_ui as dui
from train_model import ParameterInput, TrainButton
from scenario import PredictionGraph, InputSelector
import dash_bootstrap_components as dbc
from pandas_task_static_historical import ScoreAnalyzer, TaskGetIsolation
import numpy as np

os.environ['PLOTLY_USERNAME'] = 'vnoblet'
os.environ['PLOTLY_API_KEY'] = 'ZwiC68QFsCxEAmUj7w6G'
analyser = ScoreAnalyzer()


APP_NAME = 'Dash Authentication Sample App'
APP_URL = 'http://127.0.0.1:8050/'
external_stylesheets = [
    'https://codepen.io/chriddyp/pen/bWLwgP.css',
    "https://codepen.io/rmarren1/pen/mLqGRg.css",
    {
        'href': 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css',
        'rel': 'stylesheet',
        'integrity': 'sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO',
        'crossorigin': 'anonymous'
    }
]
app = dash.Dash('Dash Hello World', external_stylesheets=external_stylesheets)
app.config.suppress_callback_exceptions = True
text_style = dict(color='#444', fontFamily='sans-serif', fontWeight=300)


data = analyser.get_data()
isolation = TaskGetIsolation().output().load()
isolation = isolation[isolation.r2 > 0.3]
isolation = isolation[isolation.nb_week > 12]
a = isolation.a.describe()
hyper_parameter_input = ParameterInput(len(data.columns)).get()
train_button = TrainButton(analyser).get()

prediction_graph_class = PredictionGraph(data)
prediction_graph = prediction_graph_class.get()
prediction_button = prediction_graph_class.get_predict_button()
input_prediction_class = InputSelector(list(data.drop("consumption", axis=1).columns))

input_prediction = input_prediction_class.get()
input_state = input_prediction_class.get_input_state()

controlpanel = dui.ControlPanel(_id="controlpanel")
controlpanel.create_group(
    group="Isolation",
    group_title="Choisir un niveau d'isolation"
)
state_select = dcc.Dropdown(
    id="isolation-dropdown",
    options=[
        {'label': "Toute isolation", 'value': "all"},
        {'label': "Très bonne isolation", 'value': "v-good"},
        {'label': "Bonne isolation", 'value': "good"},
        {'label': "Mauvaise isolation", 'value': "bad"},
        {'label': "Très mauvaise isolation", 'value': "v-bad"}
    ],
    value=0
)
controlpanel.create_group(
    group="Scenario",
    group_title="Choisir une variable de scénario"
)

controlpanel.add_element(state_select, "Isolation")
controlpanel.add_element(dcc.Dropdown(id='variable_dropdown', value="comfort_temperature", options=[{"value": col, "label": col} for col in data.drop("consumption", axis=1).columns]), "Scenario")

grid = dui.Grid(_id="grid", num_rows=12, num_cols=12, grid_padding=5)

app.layout = dui.Layout(grid=grid, controlpanel=controlpanel)
grid.add_element(col=1, row=1, width=8, height=3, element=hyper_parameter_input["e"])
grid.add_element(col=9, row=1, width=4, height=3, element=train_button["e"])
grid.add_element(col=8, row=4, width=4, height=6, element=input_prediction["e"])
grid.add_element(col=1, row=4, width=6, height=6, element=prediction_graph["e"])
grid.add_element(col=5, row=10, width=3, height=1, element=prediction_button["e"])


@app.callback(hyper_parameter_input["o"], hyper_parameter_input["i"])
def hyper_parameter_input_f(*args):
    return hyper_parameter_input["f"](*args)

@app.callback(train_button["o"], train_button["i"], train_button["s"])
def train_button_f(*args):
    return train_button["f"](*args)

@app.callback(prediction_graph["o"], prediction_graph["i"], prediction_graph["s"])
def prediction_graph_f(*args):
    return prediction_graph["f"](*args)

@app.callback(input_state, [Input(component_id="isolation-dropdown", component_property='value')])
def mean_value_isolation(isolation):
    _data = data.drop("consumption", axis=1)


    if isolation == "all":
        return list(_data.mean())
    elif isolation == "v-good":
        _data = _data[_data.a < 13000]
    elif isolation == "good":
        _data = _data[_data.a < 23000]
        _data = _data[_data.a > 13000]
    elif isolation == "bad":
        _data = _data[_data.a < 40000]
        _data = _data[_data.a > 23000]
    elif isolation == "v-bad":
        _data = _data[_data.a > 40000]
    else: None
    mean = _data.mean()
    mean.a = _data.a.mean()
    return list(mean)

def align(element_list, direction="col"):
    return html.Div(html.Div([html.Div(el, className="col-md") for el in element_list], className=direction), className="container")

app.run_server(debug=True)
