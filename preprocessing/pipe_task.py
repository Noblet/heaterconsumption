from manager.query_customize import EasyQuery, EasyTableName
from threading import Thread
import d6tflow

def convertTaskName(task_name):
    task_name = list(task_name)
    task_name = task_name[4:]
    for i, c in enumerate(task_name):
        if c.isupper() and i != 0:
            task_name[i] = c.lower()
            task_name.insert(i, "_")
        elif c.isupper() and i == 0:
            task_name[i] = c.lower()
    task_name = "".join(task_name)
    return task_name


class TaskPipe(Thread):
    def __init__(self, *, input_name, output_name, input_list, output_list):
        super(TaskPipe, self).__init__()
        self.input_name = input_name
        self.output_name = output_name
        self.input_task_list = self.load_task_list(input_name, input_list)
        self.output_task_list = self.load_task_list(output_name, output_list)
        self.task_list = self.input_task_list + self.output_task_list
        self.set_dependency()

    def load_task_list(self, name, task_parameter_list):
        return list(map(lambda task_parameter: self.wrap_task_name(task_parameter, name), task_parameter_list))

    def wrap_task_name(self, task_parameter, name):
        class_ = task_parameter[0]
        params_ = task_parameter[1]
        params_['name'] = name
        return class_(**params_)

    def set_dependency(self):
        for previous_task, task in zip(self.task_list[:-1], self.task_list[1:]):
            task.set_dependency(previous_task)

    def get_task(self, class_): return list(filter(lambda x: type(x).__name__ == class_.__name__, self.task_list))

    def get_last_task(self): return self.task_list[-1]

    def get_output(self): return self.task_list[-1].output().load()

    def preview(self): d6tflow.preview(self.get_last_task())

    def run(self): d6tflow.run(self.task_list[-1])

    def invalidate(self, confirm=True):
        for task in self.task_list[1:]:
            task.invalidate(confirm=confirm)


class TaskStatePipe(TaskPipe):
    pass