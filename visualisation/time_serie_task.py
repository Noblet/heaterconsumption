from preprocessing.big_query_task_historical import TaskBigQueryInOut, TaskCopyInputTable, TaskBigQueryInNoOut
import luigi
from manager.query_customize import EasyQuery
from manager.database_updater import DbManager


class TaskSumDeviceTimestampRoundNormalize(TaskBigQueryInOut):
    timestamp_inverval = luigi.parameter.IntParameter()

    def draft_query(self, sum_value_builder):
        minutes = str(self.timestamp_inverval)
        sum_value_builder.add_select("device_url, " + EasyQuery.get_round_timestamp_minute("timestamp", minutes) + " AS timestamp, SUM(value) / " + minutes + " AS value")
        sum_value_builder.add_from_name(self.input_table_name)
        sum_value_builder.add_group_by("device_url, timestamp")
        return sum_value_builder


class TaskRoguePeriod(TaskBigQueryInNoOut):
    period_max = luigi.parameter.IntParameter()

    def draft_query(self, rogue_period_builder):
        rogue_period_builder.add("UPDATE " + self.output_table_name)
        rogue_period_builder.add("SET value = null")
        rogue_period_builder.add("WHERE period > " + str(self.period_max))
        return rogue_period_builder


class TaskRogueValueInterval(TaskBigQueryInNoOut):
    value_min = luigi.parameter.IntParameter()
    value_max = luigi.parameter.IntParameter()

    def draft_query(self, rogue_value_builder):
        rogue_value_builder.add("UPDATE " + self.output_table_name)
        rogue_value_builder.add("SET value = null")
        rogue_value_builder.add("WHERE value > " + str(self.value_max) + " OR value < " + str(self.value_min))
        return rogue_value_builder


class TaskAvgDeviceTimestampRoundNormalize(TaskBigQueryInOut):
    timestamp_inverval = luigi.parameter.IntParameter()

    def draft_query(self, sum_value_builder):
        minutes = str(self.timestamp_inverval)
        sum_value_builder.add_select("device_url, " + EasyQuery.get_round_timestamp_minute("timestamp", minutes) + "AS timestamp , avg(value) AS value")
        sum_value_builder.add_from_name(self.input_table_name)
        sum_value_builder.add_group_by("device_url, timestamp")
        return sum_value_builder


class TaskSelectGatewayTimestampValue(TaskBigQueryInOut):
    def draft_query(self, sum_value_builder):
        sum_value_builder.add_select("gateway_id, timestamp, value")
        sum_value_builder.add_from_name(self.input_table_name)
        return sum_value_builder


class TaskSelectDeviceTimestampValueGateway(TaskBigQueryInOut):
    def draft_query(self, sum_value_builder):
        sum_value_builder.add_select("device_url, " + EasyQuery.get_gateway_from_device() + ", timestamp, value")
        sum_value_builder.add_from_name(self.input_table_name)
        return sum_value_builder


class TaskFilterRandomGateway(TaskBigQueryInOut):
    def draft_query(self, filter_builder):
        filter_builder.add_select_all()
        filter_builder.add_from_name(self.input_table_name)
        filter_builder.add_where("gateway_id IN (SELECT * FROM " + DbManager().get_random_gateway_list() + ")")
        return filter_builder
