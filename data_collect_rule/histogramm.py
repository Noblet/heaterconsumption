import matplotlib.pyplot as plt
import numpy as np
from manager.query_customize import QueryBuilder
import d6tflow
import pandas as pd


class CustomTimestampDiffHistogramm:

    def __init__(self, time_diff):
        self.time_diff_minute = time_diff / 60

    def plot(self, max=100, draw_percentage=True, show=True):
        fig, ax = plt.subplots()
        bar = plt.hist(self.time_diff_minute, bins=max - 1, range=(0.5, max - 0.5), color="firebrick")
        plt.title('Difference between two timestamp')
        plt.xlabel('Time (m)')
        plt.ylabel('Count')
        if draw_percentage:
            xlabel = bar[1]
            density = np.round(100 * bar[0] / len(self.time_diff_minute), 1)

            xticks = self.center_bar(xlabel)[density > 0.5]
            ypercentageticks = density[density > 0.5]
            yvalueticks = bar[0][density > 0.5]

            for i, x in enumerate(xticks):
                ax.text(x + 1, yvalueticks[i], str(ypercentageticks[i]) + "%",  va='center')
        if not show: plt.close()
        return bar

    def center_bar(self, xlabel):
        center = []
        previous = np.inf
        for i, x in enumerate(xlabel):
            if i > 0:  center.append((x + previous) / 2)
            previous = x
        return np.array(center)


class TaskGetTimeDiff(d6tflow.tasks.TaskCache):

    def run(self, name):
        queryBuild = QueryBuilder()
        queryBuild.add_select("delta_timestamp")
        queryBuild.add_from_name("`overkiz-1365.2_Data_LAB_Heater_Consumption_Prediction_Dev.2DLHCP1_" + name + "_diff`")
        time_diff = queryBuild.get()
        time_diff.to_parquet('consumption_time_diff.pq', engine="pyarrow", compression="gzip")


class TaskPlotTimeDiffConsumption(d6tflow.tasks.TaskCache):

    def requires(self):
        return TaskGetTimeDiff()

    def run(self):
        data = pd.read_parquet('consumption_time_diff.pq', engine="pyarrow")
        return data


#data = TaskPlotTimeDiffConsumption

#TaskGetTimeDiff().run()

