import os
import re
from parquet.historical_to_parquet import TaskHistoricalParquet
from parquet.historical_to_parquet import TaskQueryHistorical
from manager.bigquery_structure import EasyTableName
import datetime
from pathlib import Path
import pandas as pd

class TaskVerifyParquetCompletion:
    linux_remote_path = "/run/media/superjazz/3a55fb9b-2cc1-4b6c-acb4-1fea4cfe7970/Backup/consumption_parquet/"
    local_path = "parquet_file/historical/"
    windows_remote_path = "D:/vnoblet/consumption_parquet/"

    def run(self, last_date):
        data_file_list = list(os.listdir(self.windows_remote_path))
        data_file_list = list(filter(TaskVerifyParquetCompletion.is_pq, data_file_list))
        for variable_name in EasyTableName.historical_table_name_list:
            data_file_list_variable = list(filter(lambda file: TaskVerifyParquetCompletion.is_variable_file(file, variable_name), data_file_list))
            taskQuery = TaskQueryHistorical(variable_name)
            begin_date = taskQuery.get_min_date()
            end_date = min(taskQuery.get_max_date(), last_date)
            delta_date = (end_date - begin_date).days
            range_date = [begin_date + datetime.timedelta(days=x) for x in range(0, delta_date)]
            missing_date = list(filter(lambda date: TaskVerifyParquetCompletion.missing_file(date, data_file_list_variable), range_date))
            missing_date.sort()
            print("Missing date for " + variable_name + " are:")
            for date in missing_date:
                print(date, end="\n")

    def complete(self, last_date):
        data_file_list = list(os.listdir(self.windows_remote_path))
        data_file_list = list(filter(TaskVerifyParquetCompletion.is_pq, data_file_list))
        for variable_name in EasyTableName.historical_table_name_list:
            data_file_list_variable = list(
                filter(lambda file: TaskVerifyParquetCompletion.is_variable_file(file, variable_name), data_file_list))
            taskQuery = TaskQueryHistorical(variable_name)
            begin_date = taskQuery.get_min_date()
            end_date = min(taskQuery.get_max_date(), last_date)
            delta_date = (end_date - begin_date).days
            range_date = [begin_date + datetime.timedelta(days=x) for x in range(0, delta_date)]
            missing_date = list(
                filter(lambda date: TaskVerifyParquetCompletion.missing_file(date, data_file_list_variable),
                       range_date))
            if missing_date:
                return False
        return True

    def check_corrupted(self):
        data_file_list = list(os.listdir(self.windows_remote_path))
        corrupted = []
        for file in data_file_list:
            try:
                pd.read_parquet(self.windows_remote_path + file)
            except Exception:
                print(file)
                corrupted.append(file)
        return corrupted

    def replace_few_file(self, list_file):
        if len(list_file) <= 50:
            for file in list_file:
                os.system("rm " + self.windows_remote_path + file)
                self.get_file(file)

    def get_file(self, file):
        filename_prefix = file.split(".")[0].split("_")
        date = filename_prefix.pop(-1)
        name = "_".join(filename_prefix)
        year, month, day = date.split("-")
        date = datetime.date(int(year), int(month), int(day))
        TaskHistoricalParquet(name).run_date(date, Path(self.windows_remote_path + file))

    @staticmethod
    def missing_file(date, data_file_list_variable):
        for file in data_file_list_variable:
            if str(date) in file:
                return False
        return True

    @staticmethod
    def is_pq(string):
        if re.search(".pq", string):
            return True
        else:
            return False

    @staticmethod
    def is_variable_file(string, name):
        if re.search("^" + name, string):
            return True
        else:
            return False


TaskVerifyParquetCompletion().complete(datetime.date(2019, 5, 20))