import sys
import datetime
from pathlib import Path
from manager.query_customize import QueryBuilder
from manager.query_customize import EasyQuery
from manager.bigquery_structure import EasyTableName
import calendar
import inspect
import os
import pandas as pd
import re


class PQNodeFileName:
    def __init__(self, variable_name, date):
        self.variable_name = variable_name
        self.date = date

    def get(self):
        parquetPath = Path(inspect.getfile(PQNodeFileName)).parent
        return parquetPath.joinpath("parquet_file/historical").joinpath(self.variable_name + "_" + str(self.date) + ".pq")


class TaskQueryHistorical:

    def __init__(self, variable_name):
        self.variable_name = variable_name
        self.input_table_name = EasyTableName.get_consumption(self.variable_name)
        self.temporary_table_name = self.input_table_name

    def get_date(self, date):
        query_builder = QueryBuilder()
        query_builder.add_select_all()
        query_builder.add_from_name(self.temporary_table_name)
        query_builder.add_filter_date_from_timestamp(date)
        return query_builder.get_execute()

    def get_min_date(self):
        return EasyQuery.get_min_date(self.input_table_name)

    def get_max_date(self):
        return EasyQuery.get_max_date(self.input_table_name)

    def create_temporary_table(self, begin_date, end_date):
        suffix = begin_date.replace("-", "_") + "_" + end_date.replace("-", "_")
        temporary_table_name = "`" + EasyTableName.dev_repository + "." + self.variable_name + suffix + "`"
        query_builder = QueryBuilder()
        query_builder.add_create_replace(temporary_table_name)
        query_builder.add_select_all()
        query_builder.add_from_name(self.input_table_name)
        query_builder.add_filter_between_date_from_timestamp(begin_date, end_date)
        query_builder.get_execute()
        self.temporary_table_name = temporary_table_name

    def delete_temporary_table(self):
        EasyQuery.delete_table(self.temporary_table_name)


class TaskHistoricalParquet:
    def __init__(self, variable_name):
        self.variable_name = variable_name
        self.taskQueryHistorical = TaskQueryHistorical(self.variable_name)

    def run_date(self, date, path=None):
        print("Pulling data of " + self.variable_name + " at date " + str(date) + " ...")
        if not path:
            path = Path(PQNodeFileName(self.variable_name, date).get())
        if not(path.exists()):
            result = self.taskQueryHistorical.get_date(str(date))
            result.to_dataframe().to_parquet(path)
            print("Data pulled and converted to parquet at date " + str(date))
        else:
            print("Data already pulled")

    def run(self, begin_date=None, end_date=None):
        min_date = self.taskQueryHistorical.get_min_date()
        max_date = self.taskQueryHistorical.get_max_date()
        if not(begin_date and begin_date >= min_date):
            begin_date = min_date
        if not (end_date and end_date <= max_date):
            end_date = max_date
        delta_date = (end_date - begin_date).days + 1
        if delta_date > 0:
            range_date = [begin_date + datetime.timedelta(days=x) for x in range(0, delta_date)]
            self.taskQueryHistorical.create_temporary_table(str(begin_date), str(end_date))
            for date in range_date:
                self.run_date(date)
            #self.taskQueryHistorical.delete_temporary_table()

    def run_year(self, year):
        for month in range(1, 13):
            begin_date = datetime.date(year, month, 1)
            end_date = datetime.date(year, month, calendar.monthrange(year, month)[1])
            self.run(begin_date, end_date)


class FixParquet:

    cast_historical_table_name_list = {
        "operating_mode": str,
        "comfort_temperature": float,
        "ambient_temperature": float,
        "consumption_index": int,
        "working_rate": int,
        "setpoint_temperature": float,
        "cumul_consumption_index": int,
        "occupancy_detection": str
    }

    linux_remote_path = Path("/run/media/superjazz/3a55fb9b-2cc1-4b6c-acb4-1fea4cfe7970/Backup/consumption_parquet")
    windows_remote_path = Path("D:/vnoblet/consumption_parquet")
    local_path = Path("parquet_file/historical")

    def get_file_filter_variable(self, name):
        historical_dir = self.windows_remote_path
        data_file_list = list(os.listdir(historical_dir))
        data_file_list = list(filter(lambda x: None != re.search("(.)*" + name + "(.)*.pq$", x), data_file_list))
        return data_file_list

    def read(self, file):
        historical_dir = self.windows_remote_path
        absolute_path = historical_dir.joinpath(file)
        self.current_path = absolute_path
        return pd.read_parquet(absolute_path)

    def save(self, frame):
        frame.to_parquet(self.current_path)

    def remove_parameter_oid(self, name):
        data_file_list = self.get_file_filter_variable(name)
        print(data_file_list)
        for file in data_file_list:
            try:
                frame = self.read(file)
                try:
                    frame = frame.drop("parameter_oid", axis=1)
                    self.save(frame)
                    print(file + " : parameter_oid removed")
                except:
                    print(file + " : No parameter_oid to be removed")
            except:
                print(file + " corrupted")
    def cast_value(self):
        historical_dir = self.windows_remote_path
        data_file_list = list(os.listdir(historical_dir))
        data_file_list = list(filter(lambda x: None == re.search("(.)*consumption_index(.)*.pq$", x), data_file_list))
        print(data_file_list)
        for file in data_file_list:
            frame = pd.read_parquet(historical_dir.joinpath(file))
            type = FixParquet.cast_historical_table_name_list[EasyQuery.get_name_parameter_oid(frame["parameter_oid"][0])]
            frame.value = frame.value.astype(type)
            frame.drop("parameter_oid", axis=1)
            frame.to_parquet(historical_dir.joinpath(file))
            print(file)


    def cast_value_consumption(self):
        historical_dir = self.windows_remote_path
        data_file_list = list(os.listdir(historical_dir))
        data_file_list = list(filter(lambda x: None != re.search("(.)*consumption_index(.)*.pq$", x), data_file_list))
        print(data_file_list)
        for file in data_file_list:
            frame = pd.read_parquet(historical_dir.joinpath(file))
            type = int
            frame.value = frame.value.astype(type)
            frame.to_parquet(historical_dir.joinpath(file))
            print(file)

    def cast_value_name(self, name):
        historical_dir = self.windows_remote_path
        data_file_list = list(os.listdir(historical_dir))
        data_file_list = list(filter(lambda x: None != re.search("(.)*" + name + "(.)*.pq$", x), data_file_list))
        type = FixParquet.cast_historical_table_name_list[name]
        for file in data_file_list:
            frame = pd.read_parquet(historical_dir.joinpath(file))
            try:
                frame.drop("parameter_oid", axis=1)
            except:
                None
            try:
                frame.value = frame.value.astype(type)
            except:
                frame[name] = frame[name].astype(type)
            frame.to_parquet(historical_dir.joinpath(file))
            print(file)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        fileName, variable_name_input = sys.argv
        TaskHistoricalParquet(variable_name_input).run()
    if len(sys.argv) == 3:
        fileName, variable_name_input, year = sys.argv
        TaskHistoricalParquet(variable_name_input).run_year(int(year))
    if len(sys.argv) == 4:
        fileName, variable_name_input, begin_date_input, end_date_input = sys.argv
        begin_datetime = datetime.datetime.strptime(begin_date_input, '%Y-%m-%d').date()
        end_datetime = datetime.datetime.strptime(end_date_input, '%Y-%m-%d').date()
        TaskHistoricalParquet(variable_name_input).run(begin_datetime, end_datetime)
