from analyse.pandas_task_static_historical import TaskMergeStaticHistorical
import time
import luigi
import d6tflow
global log_level
log_level = "DEBUG"
d6tflow.settings.log_level = 'ERROR'
skip_intro = True

if not skip_intro:
    print("======== Begin BIG QUERY tasks ========")
    length = 5
    start = time.time()
    running = True
    while running:
        time.sleep(1)
        if time.time() - start >= length:
            running = False
        else:
            print("Beginning tasks in %.1f seconds!" % (length - (time.time() - start)))
    print("Displaying the graph of tasks to be processed")
    time.sleep(2)

d6tflow.preview(TaskMergeStaticHistorical())
luigi.run(["--log-level=INFO", "--local-scheduler"], main_task_cls=TaskMergeStaticHistorical())



