import itertools
import re
import d6tflow
import luigi
import numpy as np
import pandas as pd
from manager.query_customize import EasyQuery, EasyTableName
from manager.query_customize import QueryBuilder
from preprocessing.task_static import *
from sklearn.preprocessing import LabelEncoder
from preprocessing.pipe_task import TaskPipe


class CustomEncoder:
    encoder_dict = dict()

    def __init__(self, cat_cols):
        self.cat_cols = cat_cols

    def encode(self, data):
        self.encoder_dict = dict()
        for col in self.cat_cols:
            encoder = LabelEncoder()
            data[col][data[col].notna()] = encoder.fit_transform(data[col][data[col].notna()])
            self.encoder_dict[col] = encoder
        return data

    def decode(self, data):
        for col, decoder in self.decoder_dict.items():
            data[col] = decoder.inverse_transform(data[col].astype(int))
        return data

    def get_cat_cols_index(self, column):
        cat_cols_index = [i for i, x in enumerate(column) if x in self.cat_cols]
        return cat_cols_index

    def get_num_cols_index(self, column):
        cat_cols_index = [i for i, x in enumerate(column) if x not in self.cat_cols]
        return cat_cols_index

    def get_cat_cols(self):
        return self.cat_cols

    def get_decoder_dict(self, data):
        encoder_dict = dict()
        for col in self.cat_cols:
            encoder = LabelEncoder()
            encoder.fit_transform(data[col][data[col].notna()])
            encoder_dict[col] = encoder
        return encoder_dict


class TaskGetOneStatic(d6tflow.tasks.TaskPqPandas):
    name = luigi.parameter.Parameter()

    def requires(self):
        task = TaskCopyStatic(name=self.name)
        if self.name == "room":
            task = TaskUnifyRoom(name=self.name)
        elif self.name == "setup":
            task = TaskUnifySetup(name=self.name)
        return task

    def run(self):
        query_builder = QueryBuilder()
        query_builder.add_select_all()
        query_builder.add_from_name(self.input().load())
        data = query_builder.get_execute().to_dataframe()
        self.save(data)


class TaskConvertFilterLabel(d6tflow.tasks.TaskPqPandas):
    name = luigi.parameter.Parameter()
    setup = (name == "setup")

    def requires(self):
        return TaskGetOneStatic(self.name)

    def run(self):
        data = self.input().load()
        data = self.label_na(data)
        data = self.convert(data)
        data = self.filter(data)
        data = self.add_feature(data)
        data.columns = self.name + "_" + data.columns
        self.save(data)

    def filter(self, data):
        if self.name == "setup":
            data = data[data["country"] == "France"]
            data = data[data["type"] != "LABO"]
        elif self.name == "device":
            def _filter_electrical_heater(x):
                if x["controlable"]:
                    return re.match("io:AtlanticElectricalHeater", x["controlable"]) != None
                else:
                    return False

            data = data[data.apply(_filter_electrical_heater, axis=1)]
        return data

    def convert(self, data):
        if self.name == "setup":
            data["Superficie"] = data["Superficie"].transform(pd.to_numeric)
        return data

    def label_na(self, data):
        na_values = ["null", "Undefined", "Non renseigné"]
        data = data.replace(na_values, np.nan)
        if self.name == "setup":
            data["tenement_type_english"] = data["tenement_type_english"].replace("Other", np.nan)
        return data

    def add_feature(self, data):
        if self.name == "setup":
            query_builder = QueryBuilder()
            query_builder.add_select("num_departement, zone_climatique_details, zone_climatique")

            query_builder.add_from_name(EasyTableName.get_utils_table_name("climatic_zone"))
            climatic_zone = query_builder.get_execute().to_dataframe()
            zone_to_add = pd.DataFrame([["98", "H3", "H3"], ["97", "H3", "H3"], ["99", "H1a", "H1"]], columns=climatic_zone.columns)
            climatic_zone = climatic_zone.append(zone_to_add, ignore_index=True)
            data['num_departement'] = data['zip'].transform(lambda x: x[:2])
            data = pd.merge(data, climatic_zone, left_on='num_departement', right_on="num_departement", how="left")

        return data


class TaskFeatureFactory(d6tflow.tasks.TaskPqPandas):
    def requires(self): return {"setup": TaskConvertFilterLabel("setup"), "room": TaskConvertFilterLabel("room"),
                                "device": TaskConvertFilterLabel("device"), "heater": TaskConvertFilterLabel("heater")}

    def run(self):
        device = self.input()['device'].load()
        heater = self.input()['heater'].load()
        room = self.input()['room'].load()
        setup = self.input()['setup'].load()
        room = pd.merge(setup, room, left_on="setup_id", right_on="room_setup_id", how="inner")
        device = pd.merge(heater, device, left_on="heater_id", right_on="device_id", how="inner")
        device["heater_power"] = device["heater_power"].fillna(device["heater_power"].mean())

        data = pd.DataFrame([], columns=[], index=setup["setup_gateway_id"])
        data["count_room"] = self.count_room(room)
        data["count_bedroom"] = self.count_bedroom(room)
        data["count_device"] = self.count_device(device)
        data["count_connected"] = self.count_connected(device)
        data["count_wire_pilot"] = self.count_wire_pilot(device)
        relevant_model_filter = np.logical_and.reduce([device.device_model != "Interface", device.device_model != "F127", device.device_model != "Autre"])
        data["maj_model"] = self.maj_model(device[relevant_model_filter])
        data["maj_format"] = self.maj_format(device)
        data["mean_lowering_temperature"] = self.mean_lowering_temperature(device)
        data["mean_power"] = self.mean_power(device)
        data["sum_power"] = self.sum_power(device)
        data["mean_lead_time"] = self.mean_lead_time(device)
        data["std_lead_time"] = self.std_lead_time(device)
        setup_power = pd.merge(setup, pd.DataFrame(data["sum_power"]), left_on="setup_gateway_id", right_index=True,
                               how="outer")
        setup_power.index = setup_power.setup_gateway_id
        data["surface"] = self.surface(setup_power)
        self.save(data)

    def count_room(self, room): return room.groupby("setup_gateway_id")["room_id"].count()

    def count_bedroom(self, room): return room[room["room_room_type"] == "Bedroom"].groupby("setup_gateway_id")[
        "room_id"].count()

    def count_device(self, device): return device.groupby("device_gateway")["device_id"].count()

    def count_connected(self, device): return device[device[
                                                         "device_controlable"] == 'io:AtlanticElectricalHeaterWithAdjustableTemperatureSetpointIOComponent'].groupby(
        "device_gateway")["device_id"].count()

    def count_wire_pilot(self, device): return \
    device[device["device_controlable"] == 'io:AtlanticElectricalHeaterIOComponent'].groupby("device_gateway")[
        "device_id"].count()

    def maj_model(self, setup): return setup.groupby("device_gateway")["device_model"].apply(
        lambda x: x.mode()[0] if x.mode().any() else np.nan)

    def maj_format(self, heater): return heater.groupby("device_gateway")["heater_format"].apply(
        lambda x: x.mode()[0] if x.mode().any() else np.nan)

    def mean_lowering_temperature(self, heater): return heater.groupby("device_gateway")[
        "heater_lowering_temperature"].apply(lambda x: x.mean())

    def mean_power(self, heater): return heater.groupby("device_gateway")["heater_power"].apply(lambda x: x.mean())

    def sum_power(self, heater): return heater.groupby("device_gateway")["heater_power"].apply(
        lambda x: np.nan if len(x[x.notna()]) == 0 else x.sum())

    def mean_lead_time(self, heater): return heater.groupby("device_gateway")["heater_lead_time"].agg(np.mean)

    def std_lead_time(self, heater): return heater.groupby("device_gateway")["heater_lead_time"].agg(
        lambda x: 0 if len(x[x.notna()]) == 1 else x.std())

    def surface(self, setup): return TaskDimensionSurface().get_surface(setup)


class TaskDimensionSurface:

    def __init__(self):
        self.nan = str(np.nan)
        coef = EasyQuery.get_table(EasyTableName.get_utils_table_name("power_sizing"))
        default_case = []
        tenement_type_list = pd.unique(coef["tenement_type_english"])
        zone_climatique_list = pd.unique(coef["zone_climatique"])
        tenement_build_date_list = pd.unique(coef["tenement_build_date_english"])
        for type, zone_climatique in itertools.product(tenement_type_list, zone_climatique_list):
            filter_type = (coef.tenement_type_english == type)
            filter_zone_climatique = (coef.zone_climatique == zone_climatique)
            coef_ = coef[filter_type][filter_zone_climatique]["Coef"].max()
            default_case.append([type, zone_climatique, self.nan, coef_])
        for type, build_date in itertools.product(tenement_type_list, tenement_build_date_list):
            filter_type = (coef.tenement_type_english == type)
            filter_build_date = (coef.tenement_build_date_english == build_date)
            coef_ = coef[filter_type][filter_build_date]["Coef"].max()
            default_case.append([type, self.nan, build_date, coef_])
        for zone_climatique, build_date in itertools.product(zone_climatique_list, tenement_build_date_list):
            filter_zone_climatique = (coef.zone_climatique == zone_climatique)
            filter_build_date = (coef.tenement_build_date_english == build_date)
            coef_ = coef[filter_zone_climatique][filter_build_date]["Coef"].max()
            default_case.append([self.nan, zone_climatique, build_date, coef_])
        for type in tenement_type_list:
            filter_type = (coef.tenement_type_english == type)
            coef_ = coef[filter_type]["Coef"].max()
            default_case.append([type, self.nan, self.nan, coef_])
        for zone_climatique in zone_climatique_list:
            filter_zone_climatique = (coef.zone_climatique == zone_climatique)
            coef_ = coef[filter_zone_climatique]["Coef"].max()
            default_case.append([self.nan, zone_climatique, self.nan, coef_])
        for build_date in tenement_build_date_list:
            filter_build_date = (coef.tenement_build_date_english == build_date)
            coef_ = coef[filter_build_date]["Coef"].max()
            default_case.append([self.nan, self.nan, build_date, coef_])
        default_case.append([self.nan, self.nan, self.nan, coef["Coef"].max()])
        default_case_frame = pd.DataFrame(default_case, columns=coef.columns)
        coef = coef.append(default_case_frame, ignore_index=True)
        coef["type_zone_build"] = coef["tenement_type_english"] + coef["zone_climatique"] + coef[
            "tenement_build_date_english"]
        coef.index = coef["type_zone_build"]
        coef = coef["Coef"]
        self.coef = coef

    def get_coef(self):
        return self.coef

    def get_surface(self, data):
        data["type_zone_build"] = data["setup_tenement_type_english"].fillna(self.nan) + data[
            "setup_zone_climatique"].fillna(self.nan) + data["setup_tenement_build_date_english"].fillna(self.nan)
        data["coeff_dimension"] = data["type_zone_build"].transform(lambda x: self.get_coef()[x])
        data["surface_sizing"] = data["sum_power"] / data["coeff_dimension"]
        data = data.drop(["type_zone_build", "surface_sizing"], axis=1)
        return data["sum_power"] / data["coeff_dimension"]
