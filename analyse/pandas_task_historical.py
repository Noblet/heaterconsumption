import d6tflow
import pandas as pd
from manager.query_customize import QueryBuilder, EasyTableName
from preprocessing.configure_task_historical import pipe_dict


class TaskGetHistorical(d6tflow.tasks.TaskPqPandas):
    def requires(self):
        return dict([(name, pipe.get_last_task()) for name, pipe in pipe_dict.items()])

    def run(self):
        pipe_list_name = list(pipe_dict)
        for name in pipe_list_name:
            query_builder = QueryBuilder()
            query_builder.add_select_all()
            query_builder.add_from_name(self.input()[name].load())
            data_name = query_builder.get_execute().to_dataframe()
            list_columns = list(data_name.columns)
            list_columns[0] = "all_gateway"
            list_columns[1] = "all_year"
            list_columns[2] = name
            data_name.columns = pd.core.indexes.base.Index(list_columns)
            if name != pipe_list_name[0]:
                data = pd.merge(data, data_name, on=["all_gateway", "all_year"], how="outer")
            else:
                data = data_name
            data.index = data["all_gateway"]
            data = data.drop("all_gateway", axis=1)
        self.save(data)
