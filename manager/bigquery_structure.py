import re


class EasyTableName:
    historical_table_config = {
        "operating_mode": {"type": "STRING", "oid": 872546560},
        "comfort_temperature": {"type": "FLOAT64", "oid": 872546563},
        "ambient_temperature": {"type": "FLOAT64", "oid": 872546581},
        "consumption_index": {"type": "INT64", "oid": [872546582, 873005057]},
        "working_rate": {"type": "INT64", "oid": 872546598},
        "setpoint_temperature": {"type": "FLOAT64", "oid": 872546602},
        "occupancy_detection": {"type": "STRING", "oid": 872546576},
        "main_temp": {"type": "FLOAT64"},
        "main_pressure": {"type": "INT64"},
        "main_humidity": {"type": "INT64"},
        "wind_speed": {"type": "FLOAT64"},
        "main_temp_max": {"type": "FLOAT64"},
        "main_temp_mini": {"type": "FLOAT64"},
    }

    static_table_name_list = {
        "device": {'name': "dwh_device"},
        "heater": {'name': "dwh_heater"},
        "room": {'name': "dwh_room"},
        "setup": {'name': "dwh_setup"}
    }
    project_name = "atlantic-rad"
    prod_repository = "rad_consumption"
    dev_repository = "rad_consumption_dev"
    utils_repository = "rad_utils"
    bucket_repository = "atlantic-rad.rad_iot_data"
    bucket_rawdevicestate = "RAD_rawdevicestate"
    openweathermap_repository = "atlantic-opendata-in.WeatherData"
    openweathermap_table = 'weather_real_openweathermap'
    openweathermap_history_table = 'weather_real_openweathermap'
    synop = "`atlantic-opendata-in.Donnee_Synop.4ODI003_weather_France_SYNOP`"

    @staticmethod
    def get_synop():
        return EasyTableName.synop

    @staticmethod
    def get_openweathermap():
        return EasyTableName.get_any_table_name(EasyTableName.openweathermap_repository, 'weather_real_openweathermap')

    @staticmethod
    def get_openweathermap_history():
        return EasyTableName.get_any_table_name(EasyTableName.openweathermap_repository, 'weather_real_openweathermap_history')

    @staticmethod
    def _get_any_table_name(repository, table_name):
        return repository + "." + table_name

    @staticmethod
    def _get_bucket_table_name(table_name):
        return EasyTableName.bucket_repository + "." + table_name

    @staticmethod
    def _get_prod_table_name(table_name):
        return EasyTableName.project_name + "." + EasyTableName.prod_repository + "." + EasyTableName.prod_repository + "_" + table_name

    @staticmethod
    def _get_dev_table_name(table_name):
        return EasyTableName.project_name + "." + EasyTableName.dev_repository + "." + EasyTableName.dev_repository + "_" + table_name

    @staticmethod
    def _get_utils_table_name(table_name):
        return EasyTableName.project_name + "." + EasyTableName.utils_repository + "." + EasyTableName.utils_repository + "_" + table_name

    @staticmethod
    def wrap_table_name(_easy_method):
        def easy_method(*args):
            return "`" + _easy_method(*args) + "`"
        return easy_method

    @staticmethod
    def wrap_all_table_name():
        table_name_method_list = list(filter(lambda name: re.search("table_name$", name) is not None, list(EasyTableName.__dict__.keys())))
        for _method_name in table_name_method_list:
            _easy_method = getattr(EasyTableName, _method_name)
            method_name = _method_name[1:]
            setattr(EasyTableName, method_name, EasyTableName.wrap_table_name(_easy_method))

    @staticmethod
    def get_consumption(table_name):
        if table_name in EasyTableName.static_table_name_list:
            return EasyTableName.get_prod_table_name("0_" + EasyTableName.static_table_name_list[table_name]['name'])
        else:
            return EasyTableName.get_prod_table_name("0_" + table_name)

    @staticmethod
    def get_pretty_output_process_consumption(table_name):
        return EasyTableName.get_prod_table_name("1_Gateway_Year_" + table_name)

    @staticmethod
    def get_consumption_dev(table_name):
        if table_name in EasyTableName.static_table_name_list:
            return EasyTableName.get_dev_table_name("0_" + EasyTableName.static_table_name_list[table_name]['name'])
        else:
            return EasyTableName.get_dev_table_name("0_" + table_name)

    @staticmethod
    def get_bucket(table_name):
        if table_name == "rawdevicestate":
            return EasyTableName.get_bucket_table_name(EasyTableName.bucket_rawdevicestate)
        elif table_name in EasyTableName.static_table_name_list.keys():
            return EasyTableName.get_bucket_table_name(EasyTableName.static_table_name_list[table_name]['name'])
        else:
            return EasyTableName.get_bucket_table_name(table_name)

    @staticmethod
    def get_consumption_temporary(table_name):
        return EasyTableName.get_dev_table_name("_xxx_temporary_" + table_name)

EasyTableName.wrap_all_table_name()
