from manager.bigquery_structure import EasyTableName
import re
from termcolor import colored
from config import client, cfg
from collections import OrderedDict

class EasyQuery:

    @staticmethod
    def make_method_from_easy_table_name():
        table_name_method_list = list(filter(lambda name: re.search("^get", name) is not None and re.search("table_name$", name) is not None, list(EasyTableName.__dict__.keys())))
        for method_name in table_name_method_list:
            easy_method = getattr(EasyTableName, method_name)
            setattr(EasyQuery, method_name, easy_method)

    @staticmethod
    def add_suffix(table_name, suffix):
        return "`" + table_name.split("`")[1] + "_" + suffix + "`"

    @staticmethod
    def delete_table(table_name):
        client.delete_table(table_name)

    @staticmethod
    def get_field(input_table_name):
        query_builder = QueryBuilder()
        query_builder.add_select_all()
        query_builder.add_from_name(input_table_name)
        query_builder.add("LIMIT 1")
        return list(query_builder.get_execute(hide=True).to_dataframe().columns)

    @staticmethod
    def distinct_device():
        query_builder = QueryBuilder()
        query_builder.add("SELECT distinct(device_url)")
        query_builder.add_from_name(EasyTableName.get_bucket("device"))
        return query_builder.get_execute()

    @staticmethod
    def get_min_date(table_name):
        query_build = QueryBuilder()
        query_build.add_select("MIN(EXTRACT(DATE FROM timestamp)) AS result")
        query_build.add_from_name(table_name)
        return EasyQuery.get_one_result(query_build)

    @staticmethod
    def get_max_date(table_name):
        query_build = QueryBuilder()
        query_build.add_select("MAX(EXTRACT(DATE FROM timestamp)) AS result")
        query_build.add_from_name(table_name)
        return EasyQuery.get_one_result(query_build)

    @staticmethod
    def get_one_result(query_build):
        return query_build.get_execute().to_dataframe()['result'][0]

    @staticmethod
    def get_year_from_timestamp(partition, timestamp_name):
        case_clause = "CASE "
        for year, date in partition.items():
            begin_date, end_date = str(date['begin_date']), str(date['end_date'])
            case_clause += "WHEN "
            case_clause += "EXTRACT (DATE FROM " + timestamp_name + ") >= " + "'" + begin_date + "'" + " AND EXTRACT (DATE FROM " + timestamp_name + ") <= " + "'" + end_date + "' "
            case_clause += "THEN " + str(year) + " "
        case_clause += "END AS year"
        return case_clause

    @staticmethod
    def get_year_from_begin_end_timestamp(partition):
        case_clause = "CASE "
        for year, date in partition.items():
            begin_date, end_date = str(date['begin_date']), str(date['end_date'])
            case_clause += "WHEN "
            case_clause += "EXTRACT (DATE FROM timestamp >= '" + begin_date + "'" + " AND EXTRACT (DATE FROM timestamp) <= " + "'" + end_date + "' "
            case_clause += "EXTRACT (DATE FROM end_timestamp >= '" + begin_date + "'" + " AND EXTRACT (DATE FROM end_timestamp) <= " + "'" + end_date + "' "
            case_clause += "THEN " + str(year) + " "
        case_clause += "END AS year"
        return case_clause

    @staticmethod
    def get_span_right(partition):
        cond = ""
        for year, date in partition.items():
            begin_partition, end_partition = str(date['begin_date']), str(date['end_date'])
            cond += " WHEN end_timestamp > " + EasyQuery.get_timestamp(end_partition)
            cond += " AND timestamp >= " + EasyQuery.get_timestamp(begin_partition)
            cond += " AND timestamp <= " + EasyQuery.get_timestamp(end_partition)
            cond += " THEN TIMESTAMP_DIFF( " + EasyQuery.get_timestamp(end_partition) + ",timestamp, MINUTE) * value / period"
        return cond

    @staticmethod
    def get_span_left(partition):
        cond = ""
        for year, date in partition.items():
            begin_partition, end_partition = str(date['begin_date']), str(date['end_date'])
            cond += " WHEN timestamp < " + EasyQuery.get_timestamp(begin_partition)
            cond += " AND end_timestamp <= " + EasyQuery.get_timestamp(end_partition)
            cond += " AND end_timestamp >= " + EasyQuery.get_timestamp(begin_partition)
            cond += " THEN TIMESTAMP_DIFF(end_timestamp, " + EasyQuery.get_timestamp(begin_partition) + ", MINUTE) * value /period"
        return cond

    @staticmethod
    def case_then_span_right_left(partition):
        cond_then = "CASE"
        cond_then += " " + EasyQuery.get_span_right(partition)
        cond_then += " " + EasyQuery.get_span_left(partition)
        cond_then += " ELSE value END AS value"
        return cond_then

    @staticmethod
    def get_timestamp(timestamp):
        return "TIMESTAMP('" + timestamp + "')"

    @staticmethod
    def get_round_timestamp_minute(timestamp, minute):
        return "TIMESTAMP_SECONDS(" + minute + "*60 * DIV(UNIX_SECONDS(" + timestamp + "), " + minute + " * 60))"

    @staticmethod
    def get_gateway_from_device():
        return "SPLIT(SPLIT(device_url, 'io://')[OFFSET(1)], '/')[OFFSET(0)] AS gateway_id"

    @staticmethod
    def get_mix_device_timestamp(self, time_minute):
        return "CONCAT(device_url, STRING(TIMESTAMP_SECONDS(" + time_minute + " * 60 * DIV(UNIX_SECONDS(timestamp), " + time_minute + "* 60))))"

    @staticmethod
    def get_table(input_table_name):
        builder = QueryBuilder()
        builder.add_select_all()
        builder.add_from_name(input_table_name)
        return builder.get_execute().to_dataframe()

    @staticmethod
    def get_fast_select(input_table_name, select_field):
        query_builder = QueryBuilder()
        query_builder.add_select(", ".join(select_field))
        query_builder.add_from_name(input_table_name)
        return query_builder.get()

    @staticmethod
    def drop_table(input_table_name):
        builder = QueryBuilder()
        builder.add("DROP TABLE")
        builder.add(input_table_name)
        builder.get_execute()


class QueryBuilder:
    def __init__(self):
        self.query = ""
        self.sep = "  "

    def __str__(self): return self.get().replace(self.sep, "\n")

    def copy(self):
        copy = QueryBuilder()
        copy.query = self.query
        return copy

    def add(self, line): self.query += self.sep + line

    def add_small(self, line): self.query += " " + line

    def add_create_replace(self, output_table): self.add("CREATE OR REPLACE TABLE " + output_table + " AS")

    def add_create_replace_partition_cluster(self, output_table, partition, cluster): self.add("CREATE OR REPLACE TABLE " + output_table + " PARTITION BY DATE(" + partition + ") CLUSTER BY " + cluster + " AS")

    def add_select(self, clause): self.add("SELECT " + clause)

    def add_select_from_list(self, list): self.add_select(", ".join(list))

    def add_select_all(self): self.add_select("*")

    def add_safe_select(self, *, select_field, all_field): self.add_select_from_list(", ".join(list(OrderedDict.fromkeys(select_field + all_field).keys())))

    def add_select_historical_field(self): self.add_select("device_url, timestamp, value")

    def add_select_historical_field_cast(self, type): self.add_select("device_url, timestamp, CAST(value AS " + type + ") AS value")

    def add_select_weather_field_cast(self, name, type): self.add_select("gateway_id, timestamp, CAST(" + name + " AS " + type + ") AS value")

    def add_select_historical_field_parameter_oid(self): self.add_select("device_url, parameter_oid, timestamp, value")

    def add_from_name(self, input_table_name): self.add("FROM " + input_table_name)

    def add_from(self, input_table): self.add("FROM (" + input_table + ")")

    def add_where(self, clause=None):
        if clause is None:
            clause = ""
        self.add("WHERE " + clause)

    def add_or(self): self.add("OR")

    def add_and(self): self.add("AND")

    def add_groupby(self, clause): self.add("GROUP BY " + clause)

    def add_update(self, table_name): self.add("UPDATE " + table_name)

    def add_set(self, clause): self.add("SET " + clause)

    def add_when(self, clause): self.add("WHEN " + clause)

    def add_union_all(self):
        self.add("UNION ALL")

    def add_on(self, clause): self.add("ON " + clause)

    def add_order_by(self, clause): self.add("ORDER BY " + clause)

    def add_group_by(self, clause): self.add("GROUP BY " + clause)

    def add_join(self, clause): self.add("INNER JOIN " + clause)

    def add_right_join(self, clause): self.add("RIGHT JOIN " + clause)

    def add_left_join(self, clause): self.add("LEFT JOIN " + clause)

    def add_full_outer_join(self, clause): self.add("FULL OUTER JOIN " + clause)

    def add_full_outer_join_table(self, table): self.add("FULL OUTER JOIN (" + table + ")")

    def add_device_url_filter(self, device_url): self.add_small("device_url='" + device_url + "'")

    def add_gateway_id_filter_on_device_url(self, gateway_id): self.add_small("device_url LIKE 'io://" + gateway_id + "/%'")

    def add_device_url_table_filter(self, table): self.add_small("device_url IN (SELECT device_url FROM " + table + ")")

    def add_filter_date_from_timestamp(self, date): self.add_small("EXTRACT(DATE FROM timestamp) = '" + date + "'")

    def add_filter_between_date_from_timestamp(self, begin_date, end_date): self.add_small("EXTRACT(DATE FROM timestamp) >= '" + begin_date + "'" + " AND EXTRACT(DATE FROM timestamp) <= '" + end_date + "'")

    def add_filter_between_timestamp_from_begin_timestamp_end_timestamp(self, begin_timestamp, end_timestamp): self.add_small("last_timestamp >= '" + begin_timestamp + "'" + " AND begin_timestamp <= '" + end_timestamp + "'")

    def add_filter_between_date_from_begin_end_timestamp(self, begin_date, end_date): self.add_small("EXTRACT(DATE FROM end_timestamp) >= '" + begin_date + "'" + " AND EXTRACT(DATE FROM begin_timestamp) <= '" + end_date + "'")

    def add_filter_between_date_from_timestamp_from_partition(self, partition_year):
        begin = list(map(lambda x: x[1]['begin_date'], partition_year.items()))
        end = list(map(lambda x: x[1]['end_date'], partition_year.items()))
        self.add_small("EXTRACT(DATE FROM timestamp) >= '" + min(begin) + "'" + " AND EXTRACT(DATE FROM timestamp) <= '" + max(end) + "'")

    def add_filter_before_date_from_timestamp(self, days): self.add("DATE_DIFF(CURRENT_DATE('Europe/Paris'), DATE(timestamp), DAY) > + " + str(days))

    def add_filter_parameter_oid(self, parameter_oid):
        if type(parameter_oid).__name__ == 'list':
            self.add_small("(")
            self.add_small("parameter_oid = " + str(parameter_oid[0]))
            for oid in parameter_oid[1:]:
                self.add_or()
                self.add_small("parameter_oid = " + str(oid))
            self.add_small(")")
        else:
            self.add("parameter_oid = " + str(parameter_oid))

    def get(self): return self.query

    def print_debug_info(self):
        if cfg['bigquery']['log_level'] == "DEBUG":
            print(colored("DEBUG: Running query \n" + str(self), 'blue'))
        elif cfg['bigquery']['log_level'] == "INFO":
            print(colored("INFO: Running query \n" + self.get(), 'blue'))

    def print_error(self):
        print(colored("\nQuery crashed. Run the following line with a SQL compiler: \n" + self.get(), 'blue'))

    def execute(self):
        self.get_execute()

    def get_execute(self):
        self.print_debug_info()
        try:
            return client.query(self.query).result()
        except Exception:
            self.print_error()
            raise


EasyQuery.make_method_from_easy_table_name()
