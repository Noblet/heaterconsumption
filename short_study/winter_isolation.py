from sklearn.decomposition import PCA
from preprocessing.big_query_task_historical import *
from preprocessing.pipe_task import *
from preprocessing import dataflow_task
from preprocessing import weather_task, categoric_task, mode_task, consumption_task
from threading import Thread
import sklearn
import pandas as pd
import matplotlib.pyplot as plt
from analyse.pandas_task_static import TaskConvertFilterLabel
import random
import itertools
import numpy as np
from numpy.polynomial.polynomial import polyfit

from sklearn.linear_model import LinearRegression

def get_week_day(date_string):
    year, month, day = date_string.split("-")
    week = datetime.date(int(year), int(month), int(day)).isocalendar()[1]
    return week


class TaskFilter2019(TaskBigQueryInOut):
    def draft_query(self, filter_week_builder):
        filter_week_builder.add_select_all()
        filter_week_builder.add_from_name(self.input_table_name)
        filter_week_builder.add_where("timestamp >= '2018-08-01'")
        return filter_week_builder


class TaskFilterWinterWeek(TaskBigQueryInOut):
    last_week = luigi.parameter.IntParameter()
    first_week = luigi.parameter.IntParameter()

    def draft_query(self, filter_week_builder):
        filter_week_builder.add_select_all()
        filter_week_builder.add_from_name(self.input_table_name)
        filter_week_builder.add_where("week <= " + str(self.last_week) + " OR week >= " + str(self.first_week))
        return filter_week_builder


class TaskLabelWeek(TaskBigQueryInOut):
    def draft_query(self, label_builder):
        label_builder.add_select("*, EXTRACT(WEEK FROM timestamp) AS week")
        label_builder.add_from_name(self.input_table_name)
        return label_builder


class TaskWeightAverageDeviceWeek(TaskBigQueryInOut):
    def draft_query(self, average_value_builder):
        average_value_builder.add_select("device_url, week, SUM(period * value) / SUM(period) AS value")
        average_value_builder.add_from_name(self.input_table_name)
        average_value_builder.add_group_by("device_url, week")
        return average_value_builder


class TaskWeightAverageGatewayWeek(TaskBigQueryInOut):
    def draft_query(self, average_value_builder):
        average_value_builder.add_select("gateway_id, week, SUM(period * value) / SUM(period) AS value")
        average_value_builder.add_from_name(self.input_table_name)
        average_value_builder.add_group_by("gateway_id, week")
        return average_value_builder


class TaskSelectDistinctTimestampRoundDeviceWeek(TaskBigQueryInOut):
    def draft_query(self, query_builder):
        query_builder.add_select("DISTINCT timestamp_round, device_url, week")
        query_builder.add_from_name(self.input_table_name)
        return query_builder


class TaskAverageDeviceWeek(TaskBigQueryInOut):
    def draft_query(self, average_value_builder):
        average_value_builder.add_select("device_url, week, avg(value) AS value")
        average_value_builder.add_from_name(self.input_table_name)
        average_value_builder.add_group_by("device_url, week")
        return average_value_builder


class TaskCountTimestampRoundDeviceWeek(TaskBigQueryInOut):
    def draft_query(self, query_builder):
        query_builder.add_select("device_url, week, count(timestamp_round) AS value")
        query_builder.add_from_name(self.input_table_name)
        query_builder.add_group_by("device_url, week")
        return query_builder


class TaskSumDeviceWeek(TaskBigQueryInOut):
    def draft_query(self, average_value_builder):
        average_value_builder.add_select("device_url, week, SUM(value) AS value")
        average_value_builder.add_from_name(self.input_table_name)
        average_value_builder.add_group_by("device_url, week")
        return average_value_builder


consumption_task_pipe = [
    (TaskFilter2019, {}),
    (TaskDiffValue, {}),
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (consumption_task.TaskRogueValueInterval, {"value_min": 0, "value_max": 48000}),
    (TaskLabelWeek, {}),
    (TaskFilterWinterWeek, {"first_week": 37, "last_week": 23}),
    (TaskSumDeviceWeek, {})
]

working_rate_task_pipe = [
    (TaskRemoveDoublonValue, {}),
    (TaskFilter2019, {}),
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (TaskSetWorkingRateStandByAfterDoublonLeadOver, {}),
    (TaskRogueValueInterval, {"value_min": 0, "value_max": 100}),
    (TaskLabelWeek, {}),
    (TaskFilterWinterWeek, {"first_week": 37, "last_week": 23}),
    (TaskWeightAverageDeviceWeek, {})
]

ambient_temperature_task_pipe = [
    (TaskFilter2019, {}),
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (TaskRoguePeriod, {"period_max": 250}),
    (TaskRogueValueInterval, {"value_min": 0, "value_max": 40}),
    (TaskLabelWeek, {}),
    (TaskFilterWinterWeek, {"first_week": 37, "last_week": 23}),
    (TaskWeightAverageDeviceWeek, {})
]

main_temp_task_pipe = [
    (TaskFilter2019, {}),
    (TaskLeadOverGatewayTimestamp, {"last_timestamp_period_value": 10}),
    (TaskRoguePeriod, {"period_max": 250}),
    (TaskRemoveOpenWeatherMapDoublon, {}),
    (TaskLabelWeek, {}),
    (TaskFilterWinterWeek, {"first_week": 37, "last_week": 23}),
    (TaskWeightAverageGatewayWeek, {})
]

dataflow_task_pipe = [
    (TaskFilter2019, {}),
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (TaskRoguePeriod, {"period_max": 250}),
    (TaskRoundTimestamp, {"timestamp_inverval": 240}),
    (TaskLabelWeek, {}),
    (TaskFilterWinterWeek, {"first_week": 37, "last_week": 23}),
    (TaskSelectDistinctTimestampRoundDeviceWeek, {}),
    (TaskCountTimestampRoundDeviceWeek, {})
]

intern_list = [
    TaskPipe("ambient_temperature", "ambient_temperature_week", ambient_temperature_task_pipe),
    TaskPipe("working_rate", "working_rate_week", working_rate_task_pipe),
    TaskPipe("main_temp", "main_temp_week", main_temp_task_pipe),
    TaskPipe("rawdevicestate", "dataflow_week", dataflow_task_pipe),
    TaskPipe("consumption", "consumption_week", consumption_task_pipe)
]


class TaskGetTableWeek(d6tflow.tasks.TaskPqPandas):
    name = luigi.Parameter()

    def run(self):
        ambient_name = EasyQuery.get_pretty_output_process_consumption_table_name(self.name)
        data = EasyQuery.get_table(ambient_name)
        data.columns = ['device_url', 'week', self.name]
        self.save(data)


class TaskMergeWeek(d6tflow.tasks.TaskPqPandas):
    def requires(self): return {"dataflow": TaskGetTableWeek("dataflow_week"),
                                "ambient": TaskGetTableWeek("ambient_temperature_week"),
                                "consumption": TaskGetTableWeek("consumption_week"),
                                "working_rate": TaskGetTableWeek("working_rate_week"),
                                "heater": TaskConvertFilterLabel("heater")
                                }

    def run(self):
        ambient = self.input()["ambient"].load()
        working_rate = self.input()["working_rate"].load()
        working_rate.working_rate_week = working_rate.working_rate_week / 100.
        consumption = self.input()["consumption"].load()
        dataflow = self.input()["dataflow"].load()
        heater = self.input()["heater"].load()
        heater["device_url"] = heater.heater_id.apply(lambda x: x.split("#")[0])
        dataflow.columns = ['device_url', 'week', TaskMergeWeek().requires()["dataflow"].name]
        data = pd.merge(ambient, working_rate, how="inner", left_on=["device_url", "week"], right_on=["device_url", "week"])
        data = pd.merge(data, consumption, how="inner", left_on=["device_url", "week"], right_on=["device_url", "week"])
        data = pd.merge(data, dataflow, how="inner", left_on=["device_url", "week"], right_on=["device_url", "week"])
        self.save(data)

