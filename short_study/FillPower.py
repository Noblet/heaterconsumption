import d6tflow
from short_study.winter_isolation import *


class TaskGetConsumptionPower(d6tflow.tasks.TaskPqPandas):
    def requires(self): return {"heater": TaskConvertFilterLabel("heater"), "consumption": TaskMergeWeek()}

    def run(self):
        heater = self.input()["heater"].load()
        consumption = self.input()["consumption"].load()
        heater["device_url"] = heater.heater_id.apply(lambda x: x.split("#")[0])
        power = heater[["device_url", "heater_power"]]
        consumption = pd.merge(consumption, power, how="left", on="device_url")
        consumption.index = consumption.device_url
        consumption = consumption.drop("device_url", axis=1)
        ordered = list(np.array(range(37, 53)) - 36) + list(np.array(range(0, 24)) + 17)
        not_ordered = list(np.array(range(37, 53))) + list(np.array(range(0, 24)))
        consumption.week = consumption.week.transform(lambda x: dict(zip(not_ordered, ordered))[x])
        week_hour = 24 * 7
        consumption.working_rate_week = consumption.working_rate_week * week_hour
        self.save(consumption)

    def get_random_device(self):
        data = self.output().load()
        device_url = data.index.unique()
        rand_url = random.choice(device_url)
        rand_data = data.loc[[rand_url]]
        return rand_data

    def clear_data(self, ex):
        ex = ex[ex.dataflow_week >= 30]
        ex = ex[ex.working_rate_week != 0]

        power = ex.consumption_week / ex.working_rate_week
        filter_limit = np.logical_and(power < power.quantile(0.8), power > power.quantile(0.2))
        if filter_limit.sum() >= 5:
            ex = ex[filter_limit]
        return ex

    def get_estimated_power(self, ex):
        ex = self.clear_data(ex)
        if ex.shape[0] >= 3:
            a_w, r2_w = self.get_over_working_rate_regression(ex)
            a_c, r2_c = self.get_over_consumption_regression(ex)
            return (a_w + a_c) / 2, (r2_c + r2_w) / 2
        else: return np.nan, np.nan

    def get_estimated_power(self, ex):
        ex = self.clear_data(ex)
        if ex.shape[0] >= 3:
            a_w, r2_w = self.get_over_working_rate_regression(ex)
            a_c, r2_c = self.get_over_consumption_regression(ex)
            return (a_w + a_c) / 2, (r2_c + r2_w) / 2
        else:
            return np.nan, np.nan

    def get_regression(self, x, y):
        reg = LinearRegression(fit_intercept=False)
        x = x.values.reshape(-1, 1)
        y = y.values.reshape(-1, 1)
        reg = reg.fit(x, y)
        return reg.coef_[0][0], reg.score(x, y)

    def get_over_working_rate_regression(self, ex):
        x, y = ex.working_rate_week, ex.consumption_week
        a, r2 = self.get_regression(x, y)
        return a, r2

    def get_over_consumption_regression(self, ex):
        x, y = ex.consumption_week, ex.working_rate_week
        a, r2 = self.get_regression(x, y)
        return 1/a, r2

    def plot(self, ex):
        ex = self.clear_data(ex)
        plt.xlim(0, 80)
        plt.ylim(0, 100000)
        y = ex.consumption_week
        x = ex.working_rate_week
        plt.scatter(x, y, s=1)
        a, b = self._plot_over_consumption(ex)
        print(a, b)
        a, b = self._plot_over_working_rate(ex)
        print(a, b)
        pca = PCA(n_components=2)
        pca.fit(ex[["working_rate_week", "consumption_week"]])
        vector = pca.components_
        print(vector[0][1] / vector[0][0])
        print(ex.heater_power.iloc[0])
        for length, vector in zip(pca.explained_variance_, pca.components_):
            v = vector * 3 * np.sqrt(length)
            self.draw_vector(pca.mean_, pca.mean_ + v)
        return ex

    @staticmethod
    def draw_vector(v0, v1, ax=None):
        ax = ax or plt.gca()
        arrowprops = dict(arrowstyle='->',
                          linewidth=2,
                          shrinkA=0, shrinkB=0)
        ax.annotate('', v1, v0, arrowprops=arrowprops)
        return ax

    def _plot_over_working_rate(self, ex):
        y, x = ex.consumption_week, ex.working_rate_week
        a, b = self.get_over_working_rate_regression(ex)
        plt.plot(x, a * x, '-')
        return a, b

    def _plot_over_consumption(self, ex):
        y, x = ex.consumption_week, ex.working_rate_week
        a, b = self.get_over_consumption_regression(ex)
        plt.plot(x, a * x, '-')
        return a, b

    def mygrouper(self, n, iterable):
        args = [iter(iterable)] * n
        return ([e for e in t if e != None] for t in itertools.zip_longest(*args))

